import rootReducer from "./reducers";
import {
  addToCart,
  removeFromCart,
  toggleFavorite,
  setProducts,
  setModalStatus,
  clearCart,
} from "./actions";

describe("reducers", () => {
  it("should handle addToCart action", () => {
    const initialState = {
      cart: [],
    };
    const product = { id: 1, name: "Test Product" };
    const nextState = rootReducer(initialState, {
      type: "addToCart",
      payload: product,
    });
    expect(nextState.cart).toContainEqual(product);
  });

  it("should handle removeFromCart action", () => {
    const testState = {
      cart: [{ id: 1, name: "Test Product" }],
    };
    const nextState = rootReducer(testState, {
      type: "removeFromCart",
      payload: 1,
    });
    expect(nextState.cart).toEqual([]);
  });
});
