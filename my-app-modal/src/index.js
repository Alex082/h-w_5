import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import store from "./redux/store";
import App from "./components/App/App";
import { ViewModeProvider } from "./components/API/ViewModeContext";

import "./index.css";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <ViewModeProvider>
      <Provider store={store}>
        <App />
      </Provider>
    </ViewModeProvider>
  </React.StrictMode>
);
