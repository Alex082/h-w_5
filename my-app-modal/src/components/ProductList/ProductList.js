import React from "react";
import { useViewMode } from "../API/ViewModeContext";
import ProductCard from "../ProductCard/ProductCard";
import TableContainer from "../TableContainer/TableContainer";
import "./ProductList.scss";

const ProductList = ({ products, showRemoveIcon }) => {
  const { viewMode } = useViewMode();

  const columns = React.useMemo(
    () => [
      {
        Header: "Name Product",
        accessor: "name",
      },
      {
        Header: "Price Product",
        accessor: "price",
      },
      {
        Header: "Image Product",
        accessor: "image",

        Cell: ({ value }) => (
          <img src={value} alt="Product" className="product-image-table" />
        ),
      },
      {
        Header: "Article Product",
        accessor: "article",
        Cell: ({ value }) => (
          <span style={{ display: "block", textAlign: "center" }}>{value}</span>
        ),
      },
      {
        Header: "Color Product",
        accessor: "color",
        Cell: ({ value }) => (
          <span style={{ display: "block", textAlign: "center" }}>{value}</span>
        ),
      },
    ],
    []
  );

  return (
    <>
      {viewMode === "flex" ? (
        <div style={{ display: "flex" }} className="product-list">
          {products.map((product) => (
            <ProductCard
              key={product.id}
              product={product}
              showRemoveIcon={showRemoveIcon}
            />
          ))}
        </div>
      ) : (
        <TableContainer columns={columns} data={products} />
      )}
    </>
  );
};

export default ProductList;
