import React from "react";
import { render, fireEvent } from "@testing-library/react";
import ConfirmationModal from "./ConfirmationModal";

test("ConfirmationModal component shows content and handles confirm/cancel", () => {
  const product = { id: 1, name: "Test Product" };
  const isOpen = true;
  const mockOnCancel = jest.fn();
  const mockOnConfirm = jest.fn();

  const { getByText } = render(
    <ConfirmationModal
      product={product}
      isOpen={isOpen}
      onCancel={mockOnCancel}
      onConfirm={mockOnConfirm}
    />
  );

  const textElement = getByText(
    `Вы действительно хотите удалить товар "${product.name}" из корзины?`
  );
  expect(textElement).toBeInTheDocument();

  const okButton = getByText("Ok");
  const noButton = getByText("No");

  fireEvent.click(okButton);
  expect(mockOnConfirm).toHaveBeenCalledTimes(1);

  fireEvent.click(noButton);
  expect(mockOnCancel).toHaveBeenCalledTimes(1);
});
