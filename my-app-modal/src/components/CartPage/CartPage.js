import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";

import ProductList from "../ProductList/ProductList";
import CheckoutForm from "../CheckoutForm/CheckoutForm";

import { removeFromCart, clearCart } from "../../redux/actions";

import "./CartPage.scss";

const CartPage = () => {
  const cart = useSelector((state) => state.cart);
  const dispatch = useDispatch();

  const handleRemoveFromCart = (product) => {
    dispatch(removeFromCart(product.id));
  };

  const handleClearCart = () => {
    dispatch(clearCart());
  };

  const handleCheckout = async(formData) => {
    setTimeout(() => {
      console.clear();

      console.log("Checkout Information:");
      console.log("Products:", cart);
      console.log("Form Data:", formData);

      dispatch(clearCart());
    });
  };

  return (
    <div className="cart-page">
      <div className="cart-items">
        <ProductList products={cart} showRemoveIcon />
      </div>
      <div className={`cart-form ${cart.length === 0 ? "full-width" : ""}`}>
        {cart.length > 0 ? (
          <>
            <CheckoutForm initialValues={{}} onCheckout={handleCheckout} cart={cart}/>
          </>
        ) : (
          <p>Корзина пуста. Добавьте товары перед оформлением заказа.</p>
        )}
      </div>
    </div>
  );
};

export default CartPage;
