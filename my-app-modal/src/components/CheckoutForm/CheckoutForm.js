import React, { useState } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";

import "./CheckoutForm.scss";

const CheckoutForm = ({ onCheckout, cart }) => {
  const [isLoading, setIsLoading] = useState(false);

  const initialValues = {
    firstName: "",
    lastName: "",
    age: "",
    address: "",
    mobile: "",
  };

  const validationSchema = Yup.object().shape({
    firstName: Yup.string().required("Обов'язкове поле"),
    lastName: Yup.string().required("Обов'язкове поле"),
    age: Yup.number()
      .required("Обов'язкове поле")
      .positive("Повинно бути позитивним числом"),
    address: Yup.string().required("Обов'язкове поле"),
    mobile: Yup.string().required("Обов'язкове поле"),
  });

  const handleSubmit = async (values) => {
    setIsLoading(true);
    await new Promise((res) => setTimeout(res, 2000));

    onCheckout({
      formData: values,
      cart: cart,
    });

    setIsLoading(false);
  };

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={handleSubmit}
    >
      <Form className="form">
        <div className="container-form">
          <Field
            type="text"
            name="firstName"
            placeholder="Введіть ім'я"
            autoComplete="off"
          />
          <ErrorMessage name="firstName" component="div" className="error" />

          <Field
            className="input"
            type="text"
            name="lastName"
            placeholder="Введіть прізвище"
            autoComplete="off"
          />
          <ErrorMessage name="lastName" component="div" className="error" />

          <Field
            className="input"
            type="number"
            name="age"
            placeholder="Введіть вік"
            autoComplete="off"
          />
          <ErrorMessage name="age" component="div" className="error" />

          <Field
            className="input"
            type="text"
            name="address"
            placeholder="Введіть адресу"
            autoComplete="off"
          />
          <ErrorMessage name="address" component="div" className="error" />

          <Field
            className="input"
            type="text"
            name="mobile"
            placeholder="Введіть мобільний телефон"
            autoComplete="off"
          />
          <ErrorMessage name="mobile" component="div" className="error" />
        </div>
        <button className="checkout" type="submit" disabled={isLoading}>
          {isLoading ? "Loading...." : "Checkout"}
        </button>
      </Form>
    </Formik>
  );
};

export default CheckoutForm;
