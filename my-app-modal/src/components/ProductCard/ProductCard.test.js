import React from "react";
import { render } from "@testing-library/react";
import ProductCard from "./ProductCard";

import * as reduxHooks from "react-redux";
jest.mock("react-redux");

test("ProductCard component matches snapshot", () => {
  const product = {
    some: jest.fn().mockReturnThis(),
    id: 1,
    name: "Test Product",
    price: 100,
    image: "test.jpg",
    article: "12345",
    color: "red",
  };

  jest.spyOn(reduxHooks, "useSelector").mockReturnValue(product);
  jest.spyOn(reduxHooks, "useDispatch").mockReturnValue(jest.fn());
  product.some.mockImplementation(function () {
    return true;
  });

  const showRemoveIcon = true;

  const { container } = render(
    <ProductCard product={product} showRemoveIcon={showRemoveIcon} />
  );
  expect(container).toMatchSnapshot();
});
