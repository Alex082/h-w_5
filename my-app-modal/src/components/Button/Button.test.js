import React from "react";
import { render, fireEvent } from "@testing-library/react";
import Button from "./Button";

describe("Button Component", () => {
  test("renders button text correctly", () => {
    const { getByText } = render(<Button buttonText="Test Button" />);
    const buttonElement = getByText("Test Button");
    expect(buttonElement).toBeInTheDocument();
  });

  test("calls handleButtonClick when button is clicked", () => {
    const mockHandleButtonClick = jest.fn();
    const { getByText } = render(
      <Button buttonText="Click Me" handleButtonClick={mockHandleButtonClick} />
    );
    const buttonElement = getByText("Click Me");
    fireEvent.click(buttonElement);
    expect(mockHandleButtonClick).toHaveBeenCalled();
  });
});
