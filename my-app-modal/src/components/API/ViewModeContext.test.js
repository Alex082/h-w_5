import React from "react";
import { render, act } from "@testing-library/react";
import { ViewModeProvider, useViewMode } from "./ViewModeContext";

test("ViewModeProvider toggles view mode correctly", () => {
  const TestComponent = () => {
    const { viewMode, toggleViewMode } = useViewMode();
    return (
      <div>
        <p data-testid="view-mode">{viewMode}</p>
        <button onClick={toggleViewMode}>Toggle View</button>
      </div>
    );
  };

  const { getByText, getByTestId } = render(
    <ViewModeProvider>
      <TestComponent />
    </ViewModeProvider>
  );

  const viewModeElement = getByTestId("view-mode");
  const toggleButton = getByText("Toggle View");

  expect(viewModeElement.textContent).toBe("flex");

  act(() => {
    toggleButton.click();
  });

  expect(viewModeElement.textContent).toBe("table");
});