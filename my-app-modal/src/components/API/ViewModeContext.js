import React, { createContext, useContext, useState } from "react";

const ViewModeContext = createContext();

export const useViewMode = () => {
  return useContext(ViewModeContext);
};

export const ViewModeProvider = ({ children }) => {
  const [viewMode, setViewMode] = useState("flex");

  const toggleViewMode = () => {
    // console.log(viewMode);
    setViewMode((prevMode) => (prevMode === "flex" ? "table" : "flex"));
  };

  const contextValue = {
    viewMode,
    toggleViewMode,
  };

  return (
    <ViewModeContext.Provider value={contextValue}>
      {children}
    </ViewModeContext.Provider>
  );
};
